TableBuilder
============

This project provides a the code for building and querying a discrete log look-up table. This is used for decoding homomorphically packed candidate ciphers in the vVote Verifiable Election System. 

For further details of the theory behind the approach see the paper "Solving the Discrete Logarithm Problem for Packing Candidate Preferences", available from: [http://www.computing.surrey.ac.uk/personal/st/S.Schneider/papers/2013/mycrosen2013.pdf](http://www.computing.surrey.ac.uk/personal/st/S.Schneider/papers/2013/mycrosen2013.pdf).

Inside the demo folder you will find a pre-built JAR of the code and some shell scripts for running some small example tables. In a real-world scenario the size of the table being build could run to hundreds of gigabytes and could take days to compute.

Demo
----
The following discusses files that are located in the *demo* folder

buildtable.sh will build a sample table of 15 candidates (specified in cands.txt) with a packing size of 6. It actually builds tables for a packing of 1,2,3,4,5 and 6 in case there are partial packings. The table is outputted to ./output/testTable. You will also see the partial tables used during the build process in that folder. I've kept everything fixed for the example because the line length may vary with different parameters. The line length must be known when performing the search, so if the parameters are changed look at the last few lines of the log for table building to see what the final line length is and update the parameter that is passed to verifytable.sh. If you wish to increase the number of candidates delete cands.txt before calling buildtable.sh and it will automatically generate enough new candidate ids.

verifytable.sh will verify the table build by builttable.sh. It passes the line length and packing size, along with the number of tests to perform. The java code performs two types of test, the first is on unencrypted data, the second is on encrypted data. I'll use the java method names below to distinguish them:

testBuildTableAgainstRandomPermutations: This just checks the coverage of the table by packing random selections of candidates together and checking the result is in the table.

Each iteration of the test performs the following:-
	1. Select at random the number of candidates to pack between 1 and packing size
	2. For the number selected, select candidates at random, ensuring no duplicates
 	3. Pack those candidates together and search the table for the result
	4. Check the result is correct 

testBuildTableAgainstRandomEncryptedPermutations: This checks that the packing performed in the encrypted domain is valid and that the decrypted result is found in the table.
The test initially generates a random EC ElGamal key pair and encrypts the candidate ids.
It then performs a similar test to testBuildTableAgainstRandomPermutations, except it is operating with the encrypted values.

Each iteration of the test performs the following:-
	1. Select at random the number of candidates to pack between 1 and packing size
	2. For the number selected, select encrypted candidates at random, ensuring no duplicates
 	3. Pack those candidates cipher texts together
	4. Decrypt the result
	5. Search the table and check the found data is consistent with the selection

The actual processing is batched into encryptions, packing, decryptions, searching, checking.

