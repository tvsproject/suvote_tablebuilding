/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.collections.MappedFileArray;

/**
 * The BuilderMonitor is a command line monitor and progress utility to calculate and display progress of the table building. Due to
 * the long time table building takes it is useful to be able to track progress and get an estimate of how long is left. This is
 * performed as a CLI to allow it to be easily run on a remote server.
 * 
 * @author Chris Culnane
 * 
 */
public class BuilderMonitor implements Runnable {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(BuilderMonitor.class);

  /**
   * Calculates the number of permutations that can be generated with n candidates in batches of r
   * 
   * @param n
   *          BigInteger number of candidates
   * @param r
   *          BigInteger size of sample
   * @return BigInteger of number of permutations
   */
  public static final BigInteger calculatePermutations(BigInteger n, BigInteger r) {
    BigInteger nFact = fact(n);
    BigInteger nMinusRFact = fact(n.subtract(r));
    return nFact.divide(nMinusRFact);

  }

  /**
   * Calculates the factorial of a BigInteger
   * 
   * @param bv
   *          BigInteger value for which the factorial is required
   * @return BigInteger of the factorial of bv
   */
  private static BigInteger fact(BigInteger bv) {
    BigInteger fa = BigInteger.ONE;
    for (BigInteger inte = BigInteger.ONE; inte.compareTo(bv) <= 0; inte = inte.add(BigInteger.ONE)) {
      fa = fa.multiply(inte);
    }
    return fa;
  }

  /**
   * Converts a byte size into a human readable file size
   * 
   * @param size
   *          long value with the size of the file
   * @return String containing the size in Terabytes, Gigabytes, Megabytes, kilobytes and bytes
   */
  public static final String readableFileSize(long size) {
    if (size <= 0) {
      return "0";
    }
    final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
    int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
    return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
  }

  /**
   * Boolean to record if the process is in the sort stage
   */
  private boolean                 inSort   = false;

  /**
   * The last size
   */
  private long                    lastSize = 0;

  /**
   * The last time the size was checked
   */
  private long                    lastTime;

  /**
   * The underlying MappedFileArray that is being built
   */
  private MappedFileArray<byte[]> mappedFileArray;

  /**
   * BigInteger to hold the calculation of the total number of permutations that are going to be generated
   */
  private BigInteger              permutations;

  /**
   * Constructs a BuilderMonitor looking at the specified MappedFileArray with a specified number of candidates and packing. This
   * will cause the BuilderMonitor to calculate the total amount of work involved in building the table to allow progress to be
   * monitored.
   * 
   * @param mappedFileArray
   *          The MappedFileArray to monitor
   * @param candidates
   *          int number of candidates in the table
   * @param packing
   *          int packing size
   */
  public BuilderMonitor(MappedFileArray<byte[]> mappedFileArray, int candidates, int packing) {
    this.mappedFileArray = mappedFileArray;
    this.permutations = BigInteger.ZERO;
    BigInteger n = BigInteger.valueOf(candidates);
    for (int i = 1; i <= packing; i++) {
      this.permutations = this.permutations.add(calculatePermutations(n, BigInteger.valueOf(i)));
    }
    System.out.println("Total Permutations to Calculated:" + this.permutations.toString());
  }

  /**
   * Gets the number of permutations to be calculated
   * 
   * @return BigInteger containing total number of permutations to be calculated
   */
  public BigInteger getPermutations() {
    return this.permutations;
  }

  /**
   * Called to inform the BuilderMonitor that the build process is now in the sort part of the process
   */
  public void inSort() {
    this.inSort = true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    logger.info("Starting BuildingMonitor Thread");
    // Initialisate the monitor values
    this.lastTime = System.currentTimeMillis();
    long lastHeapVal = Long.MAX_VALUE;
    long lastSortEnd = Long.MAX_VALUE;
    while (true) {
      // Constantly monitor until the destroyed
      if (this.inSort) {
        // Calculate progress in the last time interval
        long currentHeapVal = this.mappedFileArray.getHeapifyProgress();
        long currentSortVal = this.mappedFileArray.getHeapSortEnd();
        long time = System.currentTimeMillis();
        long timeForBatch = time - this.lastTime;
        if (currentHeapVal != -1) {
          // We are in the heapify stage
          long progressInHeap = lastHeapVal - currentHeapVal;
          long timeLeft = Math.round(((double) timeForBatch / (double) progressInHeap) * currentHeapVal);
          lastHeapVal = currentHeapVal;
          this.lastTime = time;
          long hours = TimeUnit.MILLISECONDS.toHours(timeLeft);
          long minutes = TimeUnit.MILLISECONDS.toMinutes(timeLeft - TimeUnit.HOURS.toMillis(hours));
          long seconds = TimeUnit.MILLISECONDS.toSeconds(timeLeft - TimeUnit.HOURS.toMillis(hours)
              - TimeUnit.MINUTES.toMillis(minutes));
          System.out.print("\r");
          System.out.print(hours + " hours " + minutes + " minutes " + seconds + " seconds remaining in heapify");
        }
        else if (currentSortVal != 0) {
          // We are in the sort stage
          long progressInSort = lastSortEnd - currentSortVal;
          long timeLeft = Math.round(((double) timeForBatch / (double) progressInSort) * currentSortVal);

          lastSortEnd = currentSortVal;
          this.lastTime = time;
          long hours = TimeUnit.MILLISECONDS.toHours(timeLeft);
          long minutes = TimeUnit.MILLISECONDS.toMinutes(timeLeft - TimeUnit.HOURS.toMillis(hours));
          long seconds = TimeUnit.MILLISECONDS.toSeconds(timeLeft - TimeUnit.HOURS.toMillis(hours)
              - TimeUnit.MINUTES.toMillis(minutes));
          System.out.print("\r");
          System.out.print(hours + " hours " + minutes + " minutes " + seconds + " seconds remaining in sort");
        }

      }
      else {
        // We are in the building stage
        long size = this.mappedFileArray.sizeL();
        if (size > 0) {
          long done = size - this.lastSize;
          long time = System.currentTimeMillis();
          long timeForBatch = time - this.lastTime;

          BigInteger remaining = this.permutations.subtract(BigInteger.valueOf(size));

          long val = Math.round(((double) remaining.longValue() / (double) done) * timeForBatch);
          long hours = TimeUnit.MILLISECONDS.toHours(val);
          long minutes = TimeUnit.MILLISECONDS.toMinutes(val - TimeUnit.HOURS.toMillis(hours));
          long seconds = TimeUnit.MILLISECONDS.toSeconds(val - TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes));
          System.out.print("\r");
          System.out.print(hours + " hours " + minutes + " minutes " + seconds + " seconds remaining");
          this.lastSize = size;
          this.lastTime = time;
        }
      }
      try {
        Thread.sleep(30000);
      }
      catch (InterruptedException e) {
        logger.info("Thread was interrupted", e);
      }

    }

  }

}
