/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

/**
 * Represents an entry in the table with a key and value.
 * 
 * @author Chris Culnane
 * 
 */
public class TableEntry {

  /**
   * Holds the value of all but the last of the permutations (useful for looking up cached value)
   */
  private byte[] allButLast;

  /**
   * Holds the value of the last permutation - this is new permutation to add to the previously cache data
   */
  private byte[] last;

  /**
   * The data we construct for the new permutation
   */
  private byte[] data;

  /**
   * Constructs a new TableEntry using the data as the key and the combination of the allButLast and last values as the value.
   * 
   * @param data
   *          byte array containing key
   * @param allButLast
   *          byte array of all the values but the last one
   * @param last
   *          byte array of the last value
   */
  public TableEntry(byte[] data, byte[] allButLast, byte[] last) {
    this.data = data;
    this.allButLast = allButLast;
    this.last = last;
  }

  /**
   * Gets the data that was set during construction
   * 
   * @return byte array of data that was set during construction
   */
  public byte[] getData() {
    return this.data;
  }

  /**
   * Get all but the last values
   * @return byte array of all but last value
   */
  public byte[] getAllButLast() {
    return this.allButLast;
  }

  /**
   * Get the last part of the value
   * @return byte array of the last part of the value
   */
  public byte[] getLast() {
    return this.last;
  }

}
