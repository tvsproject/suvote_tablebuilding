/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import java.util.Arrays;

import uk.ac.surrey.cs.tvs.collections.MappedFileArray;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheException;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;

/**
 * Represents a virtual SubTable within the single MappedFileArray.
 * 
 * @author Chris Culnane
 * 
 */
public class SubTable {

  private int                     currentIdx = 0;
  private MappedFileArray<byte[]> mappedFileArray;
  private int                     startIdx;
  private int                     endIdx;
  private int                     blockSize;
  private byte[]                  cacheEntry;
  private byte[]                  cacheValue;
  private byte[]                  cacheKey;

  /**
   * Constructs a new virtual SubTable to be used during the building of the larger. Each SubTable represents a particular packing
   * size. It provides a way of caching the values from the previously built SubTable to improve performance in building the next
   * table. For example, the first SubTable contains packing of size 1, i.e. [1][2][3][4]. The second SubTable would contain a
   * packing of size 2 [1,2],[1,3],[1,4],[2,1]...[4,3]. When constructing any subsequent SubTable the entries from the previous
   * table can be used to save having to recalculate all the values. For example, the value for [1,2,3] is [1,2] with the addition
   * of 3. Hence when constructing the next packing size with use the previous SubTable to speed up calculations. Due to the the
   * order in which the permutations are generated and added to the SubTable the values should be sequential, however, there is a
   * search method that would still find and cache the value even if they weren't, although it would be significantly less
   * efficient.
   * 
   * @param mappedFileArray
   *          MappedFileArray that this SubTable will be part of
   * @param startIdx
   *          int start index of the SubTable
   * @param endIdx
   *          int end index of the SubTable
   * @param blockSize
   *          int blocksize of this SubTable (between 1 and n)
   */
  public SubTable(MappedFileArray<byte[]> mappedFileArray, int startIdx, int endIdx, int blockSize) {
    this.mappedFileArray = mappedFileArray;
    this.startIdx = startIdx;
    this.endIdx = endIdx;
    this.currentIdx = startIdx;
    this.blockSize = blockSize;
    // Get the first value and cache the key and value
    this.cacheEntry = this.mappedFileArray.get(this.currentIdx);
    this.cacheKey = Arrays.copyOfRange(cacheEntry, CacheFile.ENCODE_LENGTH, CacheFile.ENCODE_LENGTH + blockSize);
    this.cacheValue = Arrays.copyOfRange(cacheEntry, 0, CacheFile.ENCODE_LENGTH);
  }

  /**
   * Get a value by looking for the key. Provided that the order of the permutation generation is sequential this should be optimal,
   * it will not need to search as such, just increment the pointer. However, to cover a situation where a different permutation
   * generator is used it will automatically search the entire search table if required. This is a simple linear search and not
   * optimal, but that is considered ok because this should never happen if the correct permutation generator is being used.
   * 
   * Once the value is found it is cached since we expect multiple sequential calls for the same key to occur when building larger
   * tables
   * 
   * @param key
   *          byte array containing the key
   * @return the byte array of the value
   * @throws CacheException
   *           if the key is not found
   */
  public byte[] getValue(byte[] key) throws CacheException {
    if (Arrays.equals(key, cacheKey)) {
      return cacheValue;
    }
    else {
      int searchIdx = this.currentIdx;
      nextIdx();
      while (this.currentIdx != searchIdx) {
        this.cacheEntry = this.mappedFileArray.get(this.currentIdx);
        this.cacheKey = Arrays.copyOfRange(cacheEntry, CacheFile.ENCODE_LENGTH, CacheFile.ENCODE_LENGTH + blockSize);
        this.cacheValue = Arrays.copyOfRange(cacheEntry, 0, CacheFile.ENCODE_LENGTH);
        if (Arrays.equals(key, cacheKey)) {
          return cacheValue;
        }
        nextIdx();
      }
      throw new CacheException("Cannot find key in cache");
    }
  }

  /**
   * Calculates the next index, if necessary looping around and starting at the beginning of the SubTable
   * 
   * @return int of the next index to look at
   */
  private int nextIdx() {
    this.currentIdx++;
    if (currentIdx > this.endIdx) {
      this.currentIdx = this.startIdx;
    }
    return this.currentIdx;
  }
}
