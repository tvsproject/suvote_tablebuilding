/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import java.util.Comparator;

import uk.ac.surrey.cs.tvs.collections.MappedFileArrayEntryConverter;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;
import uk.ac.surrey.cs.tvs.votepacking.sort.ByteArrayComparator;

/**
 * Subclass of the MappedFileArrayEntryConverter to provide encoding and decoding for the table entries
 * 
 * @author Chris Culnane
 * 
 */
public class TableEntryConverter extends MappedFileArrayEntryConverter<byte[]> {

  /**
   * ByteArrayComparator to compare to byte arrays
   */
  private ByteArrayComparator byteComparator = new ByteArrayComparator();

  /**
   * int of the initial length of entries
   */
  private int                 initialLength  = -1;

  /**
   * Default constructor
   */
  public TableEntryConverter() {
    super();
  }

  /**
   * Constructs a TableEntryConverter with a specified initiallength
   * 
   * @param customInitialLength
   *          int of the initial length
   */
  public TableEntryConverter(int customInitialLength) {
    super();
    this.initialLength = customInitialLength;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(byte[] o1, byte[] o2) {
    return byteComparator.compare(o1, o2);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.collections.MappedFileArrayEntryConverter#getComparator()
   */
  @Override
  public Comparator<byte[]> getComparator() {
    return this.byteComparator;
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.collections.MappedFileArrayEntryConverter#getInitialLineLength()
   */
  @Override
  public int getInitialLineLength() {
    if (this.initialLength != -1) {
      return this.initialLength;
    }
    return CacheFile.ENCODE_LENGTH + 6;
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.collections.MappedFileArrayEntryConverter#encode(java.lang.Object)
   */
  @Override
  public byte[] encode(byte[] entry) {

    return entry;
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.collections.MappedFileArrayEntryConverter#decode(byte[])
   */
  @Override
  public byte[] decode(byte[] entry) {
    return entry;
  }

}
